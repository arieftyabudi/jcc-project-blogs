import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: {
      name: 'Login',
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: () =>
      import(/* webpackChunkName: "login" */ '../views/Login.vue'),
    meta: { auth: false },
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
    meta: { auth: true },
  },
  {
    path: '/blogs',
    name: 'Blogs',
    component: () =>
      import(/* webpackChunkName: "blogs" */ '../views/Blogs.vue'),
    meta: { auth: true },
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    component: () => import(/* webpackChunkName: "blog" */ '../views/Blog.vue'),
    meta: { auth: true },
  },
  {
    path: '/create',
    name: 'Create',
    component: () =>
      import(/* webpackChunkName: "blog" */ '../views/Create.vue'),
    meta: { auth: true },
  },
  {
    path: '/edit',
    name: 'Edit',
    component: () => import(/* webpackChunkName: "blog" */ '../views/Edit.vue'),
    meta: { auth: true },
  },
  {
    path: '/homebaru',
    name: 'HomeBaru',
    component: () =>
      import(/* webpackChunkName: "blog" */ '../views/HomeBaru.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

// router.beforeEach((to, from, next) => {
//   let authToken = localStorage.getItem('sanbercode');
//   authToken = JSON.parse(authToken);
//   let valid = authToken.auth.token;
//   let validation = valid.length;
//   if (to.name !== 'Login' && validation === 0) next({ name: 'Login' });
//   if (to.name === 'Login' && validation > 0) next({ name: 'Home' });
//   else next();
// });

router.beforeEach((to, from, next) => {
  let authToken = store.getters['auth/token'];
  let valid = authToken.length;
  if (to.name !== 'Login' && valid === 0) next({ name: 'Login' });
  if (to.name === 'Login' && valid > 0) next({ name: 'Home' });
  else next();
});

// router.beforeEach((to, from, next) => {
//   let authToken = store.getters['auth/token'];
//   let valid = authToken.length;
//   console.log(valid);
//   console.log(to.meta.auth);
//   if (to.meta.auth && authToken == null) {
//     next({ name: 'Login' });
//   } else if (!to.meta.auth && valid > 1) {
//     next({ name: 'Home' });
//   } else next();
//   console.log(authToken);
// });
export default router;
