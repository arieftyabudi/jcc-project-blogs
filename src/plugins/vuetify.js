import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

import 'material-design-icons-iconfont/dist/material-design-icons.css';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'md' || 'fa',
  },
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#f8f9f',
        secondary: '#818383',
        accent: '#2178ff',
        background: '#f8f9fe',
      },
    },
  },
});
