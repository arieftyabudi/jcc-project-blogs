import Vue from 'vue';
import VueLuxon from 'vue-luxon';
Vue.use(VueLuxon, {
  templates: {},
  input: {
    zone: 'utc',
    format: 'sql',
  },
  output: {
    zone: 'local',
    format: {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    },
    locale: null,
    relative: {
      round: true,
      unit: null,
    },
  },
});
