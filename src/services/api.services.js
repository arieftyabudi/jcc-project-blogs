import axios from "axios";

const instance = axios.create({
  baseURL: "https://demo-api-vue.sanbercloud.com/api",
});

export default instance;
