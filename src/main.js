import Vue from 'vue';
import App from './App.vue';

import router from './router';
import vuetify from './plugins/vuetify';
import axios from './plugins/axios';
import vuemeta from './plugins/vue-meta';
import store from './store';
import vueluxon from './plugins/luxon';

Vue.config.productionTip = false;

new Vue({
  vueluxon,
  router,
  vuetify,
  axios,
  vuemeta,
  store,
  render: (h) => h(App),
}).$mount('#app');
