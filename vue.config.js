module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: `
        @import "@/scss/variable.scss"
        `,
      },
    },
  },
  transpileDependencies: ['vuetify'],
};
